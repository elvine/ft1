package FT1;

import java.util.Scanner;

public class coba7guntingTali {
//	Anda akan menggunting-gunting tali sepanjang z meter menjadi beberapa buah tali sepanjang x meter. Berapa kali sedikitnya anda akan menggunting tali tersebut ?
//			Contoh: z = 4, x = 1
//			Cukup menggunting 2x (pertama, tali 4m dibagi 2 sama rata, akan didapatkan masing-masing 2m. Kemudian kedua tali 2m itu dipotong bersama sama rata, akan dihasilkan 4 tali masing-masing panjang 1m)

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner kb = new Scanner(System.in);
		
		System.out.print("Masukkan panjang tali: ");
		int z = kb.nextInt();
		System.out.print("Masukkan panjang tali akhir: ");
		int x = kb.nextInt();
		
		int jmlGunting = 0;
		if (z%2 == 0) {
			for (int i = 0; i <=z; i++) {
				if(z > x) {
					z = z/2;
					jmlGunting++;
				} 
			}
		} else if (z%2 != 0) {
			z = z-1;
			for (int i = 0; i <=z; i++) {
				if(z > x) {
					z = z/2;
					jmlGunting++;
				} 
			}
		}
		
		if (z %2 == 0) {
			System.out.println(jmlGunting);
		} else {
			//System.out.println(jmlGunting +1);
		}
	}

}
