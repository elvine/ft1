package FT1;

import java.util.Scanner;

public class coba8Enkripsi {
//	Seringkali kita melihat data yang dienkripsi terhadap informasi yang bersifat rahasia. Beragam metode enkripsi telah dikembangkan hingga saat ini. Kali ini kalian akan ditugaskan untuk membuat metode enkripsi sendiri agar informasi yang diinput menjadi aman, dengan menggunakan original alfabet yang dirotasi pada enkripsi tersebut.
//
//	Constraint :
//	-    Original Alfabet : abcdefghijklmnopqrstuvwxyz
//	-    Semua enkripsi hanya menggunakan huruf kecil
//	-    Spasi/karakter spesial tidak dianggap
//
//	Input :
//	string : mengandung data yang belum dienkripsi
//	n : jumlah huruf yang digunakan untuk merotasi original alfabet (0 <= n <= 100)
//
//	Example
//	string : ba ca
//	n : 3
//
//	Output
//	Original Alfabet : abcdefghijklmnopqrstuvwxyz
//	Alfabet yang dirotasi : defghijklmnopqrstuvwxyzabc
//	Hasil enkripsi : edfd

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		Scanner kb = new Scanner(System.in);
//		
//		System.out.print("String: ");
//		String kata = kb.nextLine();
//		System.out.print("n: ");
//		int n = kb.nextInt();
//		String [] arr= kata.toLowerCase().split("");
//		char [] arrkt = new char[arr.length];
//		char [] arr1 = new char[26];
//		char ch = 'a';
//		char wadah = 0;
//		int [] nomer = new int [arr.length];
//		for (int i = 0; i <arrkt.length -1; i++) {
//			if (arr[i].equals(" ")) {
//				
//			}else {
//				arrkt[i] = arr[i].charAt(0);
//			}
//		}
////		for(char i : arrkt) {
////			System.out.print(i+ " ");
////		}
//		
//		for (int i = 0; i <arr1.length-1; i++  ) {
//			arr1[i] = ch;
//			ch +=1;
//			
//		}
//		if( n>= 0 && n <= 100) {
//			for (int i = 0; i < n; i++) {
//				for (int en = 0; en <arr1.length - 1; en++) {
//					if (en == 0) {
//						wadah = arr1[en];
//						arr1[en] = arr1[en+1];
//					} else {
//						arr1[en] = arr1[en+1];
//					}
//					arr1[arr1.length-1] = wadah;
//				}
//			}
//			
//		} else {
//			System.out.println("Error");
//		}
////		for(char i: arr1) {
////			System.out.print(i + " ");
////		}
//		
//		for (int i = 0; i < arr1.length-1; i++) {
//			for(int z = 0; z < arrkt.length-1; z++) {
//				if (arr1[i] == arrkt[z]) {
//					System.out.println(z);
//					nomer[i] = z;
//					
//				}
//			}
//		
//			
//		}
//		System.out.println("");
		
		Scanner kb = new Scanner(System.in);
		
		String[] abj = {"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"};
		String[] abj2 = new String[abj.length];
		
		System.out.print("String: ");
		String input = kb.nextLine();
		String[] kt = input.split("");
		int [] arr = new int[kt.length];
		System.out.println();
		System.out.print("n: ");
		int n = kb.nextInt();
		String wadah = "";
		
		for (int i = 0; i < n; i++) {
			for (int x = 0; x < abj.length-1; x++) {
				wadah = abj[0];
				abj2[x] = abj[x+1];
			}
			abj2[abj.length-1] = wadah;
		}
		for (int i = 0; i <abj.length; i++) {
			for (int x = 0; x < abj2.length; x++) {
				if (abj2[x].equals(kt[i]) {
					arr[i] = x;
				}
			}
		}
		for(int i : arr) {
			System.out.print(i+" ");
		}
	}

}
